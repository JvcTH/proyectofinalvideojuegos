using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLite : MonoBehaviour
{
    public GameObject BigBullet;
    public SpriteRenderer sr;
    public float speed = 5f;
    public float starWT=1.0f;
    public float charge = 0;
    public Transform[] moveSpot;
    private float waitTime;
    private int i=0;
    private Vector2 actualPos;
    public int vidas = 5;
    public RobotController Enemy;
    void Start()
    {
        waitTime= starWT;
        Enemy= FindObjectOfType<RobotController>();
    }

    // Update is called once per frame
    void Update()
    {          
        if (charge <= 4.5)
        {charge += Time.deltaTime;}
        if (charge >= 4.5)
        {
            //var dist = 3.5f;
            var bullet1 =  BigBullet;
            var position1 = new Vector2(transform.position.x, transform.position.y);
            var rotation = BigBullet.transform.rotation;
            Instantiate(bullet1, position1, rotation);
            charge = 0;
        }
                      //mover hacia (posActual,posFinal,Velocidad)  //MoveSpot= Array de Coodenadas
        transform.position = Vector2.MoveTowards(transform.position,moveSpot[i].transform.position,speed*Time.deltaTime);
        if(Vector2.Distance(transform.position,moveSpot[i].transform.position)<0.15f)
        {   //Mantener posicion un tiempo determinado
            if(waitTime<=0){waitTime=0;}
            if(waitTime<=0) //Verifica si se paso el tiempo de espera
            {                            //Recorre las coordenadas del array
                if(moveSpot[i]!=moveSpot[moveSpot.Length-1]){ i+=1;}
                else {i=0;}
            }
            waitTime =starWT;
        }
        else{waitTime -= Time.deltaTime;} //Reduce el tiempo de espera
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("bullet"))
        {
            vidas = vidas - 1;
            if (vidas <= 0){ Destroy(this.gameObject); Enemy.RestEnemy(1); }
        }
        if (collider.gameObject.CompareTag("bulletCharge"))
        {
            vidas = vidas - 2;
            if (vidas <= 0){ Destroy(this.gameObject); Enemy.RestEnemy(1); }
        }
    }
}
