using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodigoBrillo : MonoBehaviour
{
     public Slider slider;
     public float slidervalue;
     public Image panelbrillo;
    void Start()
    {
        slider.value =PlayerPrefs.GetFloat("Brillo",0.5f);
        panelbrillo.color = new Color(panelbrillo.color.r,panelbrillo.color.g, panelbrillo.color.b,slider.value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Changeslider(float valor){
        slidervalue = valor;
        PlayerPrefs.SetFloat("Brillo",slidervalue);
        panelbrillo.color = new Color(panelbrillo.color.r,panelbrillo.color.g, panelbrillo.color.b,slider.value);
    }
}
