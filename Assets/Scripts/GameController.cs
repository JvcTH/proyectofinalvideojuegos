using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public Text enemysText;
    public Text lifesText;
    public Text timeText;
    public Text tipo1Text;
    public Text tipo2Text;
    public Text tipo3Text;


    private int _enemys = 14;
    private int _lifes = 6;
    private float _time = 360;
    private int _tipo1 = 0;
    private int _tipo2 = 0;
    private int _tipo3 = 0;

    private void Start()
    { 
        enemysText.text = "Enemigos: " + _enemys;
        lifesText.text = "Vidas: " + _lifes;
        tipo1Text.text = "Moneda oro: " + _tipo1;
        tipo2Text.text = "Moneda Bronce: " + _tipo2;
        tipo3Text.text = "Moneda Plata: " + _tipo2;
    }

    void Update()
    {
        if (timeText!=null)
        {
            _time -= Time.deltaTime;
            timeText.text = "Tiempo: " + _time;
        }
        if (_time <= 0)
        {
            SceneManager.LoadScene("Nivel2");
        }
    }

    public int GetScore()
    {return _enemys;}

    public void DestroyEnemy()
    {
        _enemys -= 1;
    }
    public void LoseLife()
    {
        _lifes -= 1;
       lifesText.text = "Vidas: " + _lifes;
    }

    public int GetLifes()
    { return _lifes;}
    public int GetEnemys()
    {return _enemys;}
      
    public void PlusTipo1(int tipo1)
    {
        _tipo1 += tipo1;
        tipo1Text.text = "Moneda Oro: " + _tipo1;
    }
    public void PlusTipo2(int tipo2)
    {
        _tipo2 += tipo2;
        tipo2Text.text = "Moneda Bronce: " + _tipo2;
    }
    public void PlusTipo3(int tipo3)
    {
        _tipo3 += tipo3;
        tipo3Text.text = "Moneda Plata: " + _tipo2;
    }
}