using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodigoVolumen : MonoBehaviour
{

    public Slider slider;
    public float slidervalue;
    public Image imagenMute;
    // Start is called before the first frame update
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumeAudio",0.5f);
        AudioListener.volume = slider.value;
        RevisarSiestoyMute();
    }
    public void ChangeSlider(float valor){
        slidervalue = valor;
        PlayerPrefs.SetFloat("volumeAudio",slidervalue);
        AudioListener.volume = slider.value;
        RevisarSiestoyMute();
    }

      public void RevisarSiestoyMute(){
        if(slidervalue == 0)
        {
            imagenMute.enabled=true;    
        }
        else{
            imagenMute.enabled = false;
        }
    }   
}
