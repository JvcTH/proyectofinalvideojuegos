using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nave1Control : MonoBehaviour
{    
    public GameObject BigBullet;
    public GameObject LiteBullet;
    public float charge = 0;
    public Transform playerTransform;
     
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {   
        var x = playerTransform.position.x;
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
        
        if (charge <= 2.5)
        {charge += Time.deltaTime;}
        if (charge >= 2.5)
        {
            var dist = .5f;
            var bullet1 =  LiteBullet;
            var bullet2 =  BigBullet;

            var position1 = new Vector2(transform.position.x+dist, transform.position.y-3);
            var position2 = new Vector2(transform.position.x-dist, transform.position.y-3);
            var position3 = new Vector2(transform.position.x+4, transform.position.y-1);
            var position4 = new Vector2(transform.position.x-4, transform.position.y-1);
            
            var rotation = LiteBullet.transform.rotation;

            Instantiate(bullet1, position1, rotation);
            Instantiate(bullet1, position2, rotation);
            Instantiate(bullet2, position3, rotation);
            Instantiate(bullet2, position4, rotation);
            charge = 0;
        }

    }
}

