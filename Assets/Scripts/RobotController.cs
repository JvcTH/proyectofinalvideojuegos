using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class RobotController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public GameObject rightBullet;
    public GameObject leftBullet;
    
    public GameObject rightBulletCharge;
    public GameObject leftBulletCharge;
    public List<AudioClip> AudioClips;
    private GameController _game;
    private AudioSource audioSource;

    public int Enemigos = 4; 

    public int VecesSalta = 0; 
    public float charge = 0;
    public float timeDeath = 0;
    public bool muerto = false;
    public int nivel = 1;
    

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (muerto)
        {
            animator.SetInteger("Estado",6);
            timeDeath += Time.deltaTime;
            if (timeDeath >= 1 )
            {
                if (nivel == 1){SceneManager.LoadScene("SampleScene");}
                if (nivel == 2) {SceneManager.LoadScene("Nivel2");}
                if(nivel != 1 && nivel != 2) {SceneManager.LoadScene("Nivel3");} 
            } 
        }
        else
        {
            animator.SetInteger("Estado", 0);
            rb.velocity = new Vector2(0, rb.velocity.y); 

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(10, rb.velocity.y); 
                sr.flipX = false;
                animator.SetInteger("Estado",1); 
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-10, rb.velocity.y); 
                sr.flipX = true; 
                animator.SetInteger("Estado",1); 
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {    
                if(VecesSalta < 2)
                {   
                animator.SetInteger("Estado",2);
                    //Saltar            
                    const int V = 30;
                    rb.AddForce(Vector2.up * V, ForceMode2D.Impulse); //Salto              
                VecesSalta +=1;  
                audioSource.PlayOneShot(AudioClips[0]); 
                }
            }   
  
            if (Input.GetKey(KeyCode.X))
            {
                charge += Time.deltaTime;
                if (sr.color == Color.green)
                {sr.color= Color.white;}
                sr.color= Color.green;
            }   
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (sr.color == Color.green)
                { sr.color= Color.white; }
                if (rb.velocity.x == 0)
                {
                    animator.SetInteger("Estado",4);
                    audioSource.PlayOneShot(AudioClips[1]);
                }
                else
                {
                    animator.SetInteger("Estado",5);
                    audioSource.PlayOneShot(AudioClips[1]);
                }
                if (charge >= 1)
                {
                    var bullet = sr.flipX ? leftBulletCharge : rightBulletCharge;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBulletCharge.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    var bullet = sr.flipX ? leftBullet : rightBullet;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }
                charge = 0;
            }
        }
    }
       private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("enemy"))
        {
            _game.LoseLife();
            if (_game.GetLifes()<=0) {muerto=true;}
        }
        if (collision.gameObject.CompareTag("suelo") && VecesSalta == 2) {VecesSalta = 0;}
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("key"))
        { SceneManager.LoadScene("Nivel2"); nivel =2;}
        if (collider.gameObject.CompareTag("key2"))
        { SceneManager.LoadScene("Nivel3");nivel =3;}
        if (Enemigos==0)
        { SceneManager.LoadScene("Final");}
        
        if (collider.gameObject.CompareTag("enemy")||collider.gameObject.CompareTag("pro2"))
        {
            _game.LoseLife();
            if(_game.GetLifes()<=0) {muerto=true;}
        }
        
        if (collider.gameObject.CompareTag("pro1"))
        {
            _game.LoseLife();
            _game.LoseLife();
            if (_game.GetLifes()<=0) {muerto=true;}
        }
        if (collider.gameObject.CompareTag("tipo1"))
        {
            Destroy(collider.gameObject);
            _game.PlusTipo1(1);
            audioSource.PlayOneShot(AudioClips[2]);
        }
        if (collider.gameObject.CompareTag("tipo2"))
        {
            Destroy(collider.gameObject);
            _game.PlusTipo2(1);
            audioSource.PlayOneShot(AudioClips[2]);
        }
        if (collider.gameObject.CompareTag("tipo3"))
        {
            Destroy(collider.gameObject);
            _game.PlusTipo3(1);
           audioSource.PlayOneShot(AudioClips[2]);
        }
    }
    public void RestEnemy(int i)
    {
        Enemigos = Enemigos-i;
    }
}